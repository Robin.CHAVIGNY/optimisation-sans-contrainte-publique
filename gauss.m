% Mathis Le Gall, Clément Karinthi

function [e,f,Jf] = gauss(x,y,a,M)

% Initialisation
n = length(x);
j = 1:M;
i = 1:3*M;
f = zeros(n,1);
Jf = zeros(n,3*M);

% Écriture matricielle pour f
a1=a(3*j-2); 
a2=a(3*j-1); 
a3=a(3*j);
X = x(:,ones(M,1));
A1 = a1(:,ones(n,1))';
A2 = a2(:,ones(n,1))';
A3 = a3(:,ones(n,1))';

% Calcul de f et de sa norme
f = sum(A1.*exp(-((X-A2)./A3).^2),2) - y;
e = norm(f);

% Écriture matricielle pour Jf
a11 = repelem(a1,3); % On répète 3x les éléments de a1 pour avoir un vecteur de taille 3*M
a22 = repelem(a2,3);
a33 = repelem(a3,3);
XX = x(:,ones(3*M,1));
A11 = a11(:,ones(n,1));
A11 = A11';
A22 = a22(:,ones(n,1));
A22 = A22';
A33 = a33(:,ones(n,1));
A33 = A33';

% Jacobienne
Jf(:,3*j) = (2*A11(:,3*j).*((XX(:,3*j)-A22(:,3*j)).^2)./(A33(:,3*j).^3)).*exp(-((XX(:,3*j)-A22(:,3*j))./A33(:,3*j)).^2);
Jf(:,3*j-1) = (2*A11(:,3*j-1).*(XX(:,3*j-1)-A22(:,3*j-1))./(A33(:,3*j-1).^2)).*exp(-((XX(:,3*j-1)-A22(:,3*j-1))./A33(:,3*j-1)).^2);
Jf(:,3*j-2) = exp(-((XX(:,3*j-2)-A22(:,3*j-2))./A33(:,3*j-2)).^2);

