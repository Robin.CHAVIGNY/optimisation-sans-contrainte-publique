function [xk,k,nf]=bfgs(x0,f,eps,itmax)
%% BFGS : résolution d'un système non linéaire 
% 
% Synopsis:
%       [xk,k,nf]=bfgs(x0,f,eps,itmax)
%       [xk,k,nf]=bfgs(x0,f,eps)
%       [xk,k,nf]=bfgs(x0,f)
%
% Inputs:
%       x0    : valeur initiale de x (vecteur)
%       f     : variable fonction du système (variable fonction, @fct)
%       eps   : précision des calculs (float)
%       itmax : nombre d'itérations max (entier)
%
% Outputs:
%       xk    : valeur final de l'algorithme (vecteur)
%       k     : nombre de tour de boucle (eniter)
%       nf    : nombre d'appel de la fonction mis en paramètre (entier)
%-------------------------------------------------------------------------

% Cas de parametre manquant
if (nargin == 2)
    eps=0.001;itmax=100;
elseif (nargin == 3)
    itmax=100;
end

% Initialisation 
[n,~]=size(x0);
nf=0;
H0=eye(n);
k=0;
xk=x0;
[fk,gk]=feval(f,xk);  nf=nf+1;
Hk=H0;

while (k<itmax && nf<5*itmax)
    it = 0;
    while(norm(gk)>eps && it<2*n*n)
        dk=-Hk*gk;
        [~,tk,~,nfun]=armijo(f,xk,fk,gk,dk); 
        nf=nf+nfun;
        hk=tk*dk;
        xk=xk+hk;
        [fk,gk1]=feval(f,xk); nf=nf+1;
        yk=gk1-gk;
        gk=gk1;
    
        if (hk'*yk>0)
            Hk=Hk-(1/(hk'*yk))*(hk*yk'*Hk+Hk*yk*hk')+(1+((yk'*Hk*yk)/(hk'*yk)))*((hk*hk')/(hk'*yk));
        end
        k=k+1;
        it=it+1;
    end   
    if (norm(gk)<=eps)
        break;
    end
end