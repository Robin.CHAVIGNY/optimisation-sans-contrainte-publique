function [fx,gf]=f31(x)

[n,~]=size(x);
if (n==2)
    fx=2*(2*x(2)-x(1))^2;
    gf=[-4*(2*x(2)-x(1));8*(2*x(2)-x(1))];
else
    fx=0;
    gf=[8*(2*x(2)-x(1))];
    for i=2:n-1
        fx=fx+i*(2*x(i)-x(i-1))^2;
        gf=[gf
            4*i*(2*x(i)-x(i-1))-2*(i+1)*(2*x(i+1)-x(i))];
    end
    fx=fx+n*(2*x(n)-x(n-1))^2;
    gf=[gf;4*i*(2*x(n)-x(n-1))];
end
