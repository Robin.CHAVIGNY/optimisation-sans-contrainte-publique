function [fx,gf]=f32(x)

[n,~]=size(x);
s1=0;
s2=0;
fx=0;
gf=[];
for i=1:n
    s1=s1+(x(i)-1)^2;
    s2=s2+(x(i))^2;
end
fx=10^(-5) *s1 +(s2-0.25)^2;
for i=1:n
    gf=[gf
        10^(-5) * 2*(x(i)-1)+4*x(i)*(s2-0.25)^2];
end