function [fx,gf]=f1(x)
fx=100*(x(2)-x(1)^2)^2 +(1-x(1))^2;
gf=[200*(x(2)-x(1)^2)*(-2*x(1))+2*(1-x(1))*(-1)
    200*(x(2)-x(1)^2)];