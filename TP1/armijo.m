function [x,tk,flag,nfun]=armijo(fun,xk,fk,gk,dk)
%% ARMIJO Armijo line search rule
% input :
% fun = cost function handle (@fct)
% xk,fk,gk,dk = xk, f(xk), grad(f(xk)), dk current search direction
% output :
% x, tk = x <- xk+tk*dk, tk the step size length
% flag = convergence flag
% 0 the Armijo scheme converges within maxit iterations
% 1 the Armijo scheme iterated maxit times (tk is too small)
% nfun = the number of function fun evaluations
%=================================

tau=0.5; sigma=10^-3;
maxit=50;
nfun=0;
gd=sigma*gk'*dk;
tk=1; flag=1;
iter=0;
while (iter < maxit)
    iter=iter+1;
    x=xk+tk*dk;
    [f,~]=feval(fun,x); nfun=nfun+1;
    if (f-fk-tk*gd <= 0)
        flag=0; break
    else
        tk=tau*tk;
    end
end