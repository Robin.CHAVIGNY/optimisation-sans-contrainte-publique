% Script LevenbergMarquardt
% Mathis Le Gall, Clément Karinthi

% Premier echantillon
x=[0.01,1.93,2.95,3.26,4.18,5.73,6.29,7.70,8.91,9.12]';
y=[0.98,0.84,0.80,0.78,0.82,0.78,0.80,0.85,0.90,0.95]';

% Deuxieme echantillon
%x = [0.9 1.3 1.9 2.1 2.6 3.0 3.9 4.4 4.7 5.0 6.0 7.0 8.0 9.2 10.5 11.3 11.6 12.0 12.6 13.0 13.3]';
%y = [1.3 1.5 1.85 2.1 2.6 2.7 2.4 2.15 2.05 2.1 2.25 2.3 2.25 1.95 1.4 0.9 0.7 0.6 0.5 0.4 0.25]';

% Troisieme echantillon
%load('dataset200.mat');
%x=x';

ne = length(x); % Taille de l'echantillon

% Declaration des variables
k=0;
% Pour le premier echantillon, utiliser:
lambda = 5;
% Pour le deuxieme et troisieme echantillon, utiliser:
%lambda=3;  
eps=0.001;
itermax=150;
M=3;
n=3*M;
a= ones(3*M,1);

err = 1; % Pour qu'on rentre une fois dans le while

% Algorithme de Levenberg Marquardt
while(err>eps && k<itermax) 
    
    
    [e_a,f,Jf] = gauss(x,y,a,M); % Calcul de l'erreur
    
    H = Jf'*Jf; % Hessienne
    d =  (H + lambda * eye(3*M))\(- Jf'*f); %
   
    [e_a_d,f_d,Jf_d] = gauss(x,y,a+d,M); 
    
    if (e_a_d<e_a) % Descente
        lambda = lambda/10;
        a=a+d;
        
    else % Sinon pas descente
        lambda = 10*lambda;
    end
    
    k=k+1;
    err=norm(Jf'*f);
    fprintf("iter=%d  lambda=%10.4e  err=%15.8e \n",k,lambda,err);

end

% Trouver les images de l'échantillon
j = 1:M;
a1=a(3*j-2); 
a2=a(3*j-1); 
a3=a(3*j);
X = x(:,ones(M,1));
A1 = a1(:,ones(ne,1))';
A2 = a2(:,ones(ne,1))';
A3 = a3(:,ones(ne,1))';
YA = sum(A1.*exp(-((X-A2)./A3).^2),2);
   
% Affichage
plot(x,y,'. blue');
hold on;
plot(x,YA,"red");