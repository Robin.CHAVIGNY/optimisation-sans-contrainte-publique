function [y]=y_xa(xi,a,M)
y=0;
for l=1:M
    y=y+a(3*l-2) * exp(-( (xi-a(3*l-1))/a(3*l) )^2);
end
