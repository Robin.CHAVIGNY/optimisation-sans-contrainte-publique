function []=LM(varargin)
%% LM : moindres carrés non linéaires
%
% Synopsis:
%           []=LM(x,y,M)
%           []=LM(x,y)
%           []=LM(M)
%
% Inputs:
%           x : vecteur composer de l'abscisse des 
%               points recherchers
%           y : vecteur composer de l'ordonnées des 
%               points recherchers
%           M : taille du vecteur de précission a
%-------------------------------------------------------

% Cas de paramètre manquant
switch nargin
    case 0
        M = 5;
        x = [0.9 1.3 1.9 2.1 2.6 3.0 3.9 4.4 4.7 5 6 7 8 9.2 10.5 11.3 11.6 12 12.6 13 13.3];
        y = [1.3 1.5 1.85 2.1 2.6 2.7 2.4 2.15 2.05 2.1 2.25 2.3 2.25 1.95 1.4 0.9 0.7 0.6 0.5 0.4 0.25];
        %x=[0.01,1.93,2.95,3.26,4.18,5.73,6.29,7.70,8.91,9.12]';
        %y=[0.98,0.84,0.80,0.78,0.82,0.78,0.80,0.85,0.90,0.95]';

    case 1
        M = varargin{1};
        x = [0.9 1.3 1.9 2.1 2.6 3.0 3.9 4.4 4.7 5 6 7 8 9.2 10.5 11.3 11.6 12 12.6 13 13.3];
        y = [1.3 1.5 1.85 2.1 2.6 2.7 2.4 2.15 2.05 2.1 2.25 2.3 2.25 1.95 1.4 0.9 0.7 0.6 0.5 0.4 0.25];
        %x=[0.01,1.93,2.95,3.26,4.18,5.73,6.29,7.70,8.91,9.12]';
        %y=[0.98,0.84,0.80,0.78,0.82,0.78,0.80,0.85,0.90,0.95]';
    case 2
        x = varargin{1};
        y = varargin{2};
        M = 5; % Valeur par défaut pour M
    case 3
        x = varargin{1};
        y = varargin{2};
        M = varargin{3};
    otherwise
        error('Trop d''arguments.');
end

% Initialisation des paramètres
a= ones(3*M,1);
itmax=1500;
k=0;
lambda = 0.001;
eps = 0.01;
err = 1;

% Boucles principale
while (err > eps && k < itmax)
    [err,fa,Jfa]=f_Jf(x,y,a,M); % calcul de l'erreur, de la somme des gaussienne et de la matrice Jacobienne
    H=(Jfa')*Jfa; % matrice Hessienne
    delta_e=-Jfa'*fa; % gradient de la norme Euclidienne
    d= (H + lambda*eye(3*M))\delta_e; % calcul de la direction de descente
    [errd,~,~]=f_Jf(x,y,a+d,M);
    if (errd<err)
        lambda=lambda/10; % Mis à jour de lambda 
        a=a+d; % Mis à jour de a
        err=errd; % Mis à jour de l'erreur
    else
        lambda=lambda*10; % Mis à jour de lambda 
    end  
    k=k+1;
end

% Visualisation de la solution
xt=min(x):1/10:max(x);
r=length(xt);
yt=[];

for i=1:r
    yt=[yt,y_xa(xt(i),a,M)];
end

plot(x,y,"*",xt,yt,'-');

    