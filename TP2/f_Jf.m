function [err,fa,Jfa]=f_Jf(x,y,a,M)

n=length(a);
p=length(x);

fa=zeros(p,1);

for i=1:p
    y2=y_xa(x(i),a,M);
    fa(i)=y2-y(i);
end

err=0.5*(fa')*fa;

Jfa=zeros(p,n);

for i=1:p
    for j=1:n
        if (mod(j,3)==1)
            Jfa(i,j)= exp(-((x(i)-a(j+1))/a(j+2))^2);
        elseif (mod(j,3)==2)
            Jfa(i,j)=a(j-1) * exp(-((x(i)-a(j))/a(j+1))^2)*2*(x(i)-a(j))/(a(j+1)^2);
        else
            Jfa(i,j)=a(j-2) * exp(-((x(i)-a(j-1))/a(j))^2)*((x(i)-a(j-1))^2)*2/((a(j))^3);
        end
    end 
end